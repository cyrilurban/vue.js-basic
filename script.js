new Vue({
  el: '#app',
  data: {
    title: 'Cyril Urban is king!',
    link: 'http://google.com',
    foundLink: '<a href="http://google.com">Google</a>',
    counter: 0,
    secondCounter: 0,
    x: 0,
    y: 0,
    name: 'Cyril',
    isRedSelected: false,
    isGreenSelected: false,
    isBlueSelected: false,
    color: 'green',
    colorAny: 'red',
    size: '100',
  },
  computed: {
    output: function () {
      console.log('computed');
      return this.counter > 10 ? 'Grater then 10' : 'Smaller then 10'
    },
    divClasses: function () {
      return {
        red: this.isRedSelected,
        blue: !this.isRedSelected
      }
    },
    colorSize: function () {
      return {
        backgroundColor: this.colorAny,
        width: this.size + "px"
      }
    }
  },
  watch: {
    counter: function() {
      const vm = this;
      setTimeout(function() {
        vm.counter = 0;
      }, 2000);
    }
  },
  methods: {
    changeTitle: function (event) {
      this.title = event.target.value;    
    },
    increaseBtn: function (step) {
      this.counter += step;
    },
    updateCoord: function (event) {
      this.x = event.clientX;
      this.y = event.clientY;
    },
    alertMe: function (event) {
      alert(event.target.value);
    },
    result: function () {
      console.log('method');
      return this.counter > 10 ? 'Grater then 10' : 'Smaller then 10'
    }
  }
})